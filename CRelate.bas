Attribute VB_Name = "CRelate"
Option Explicit
Rem ++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rem Add this function into ThisWorkBook if you like to use formula analysis double click
'Private Sub Workbook_SheetBeforeDoubleClick(ByVal Sh As Object, ByVal Target As Range, Cancel As Boolean)
'    Call CRelate.getLines(Sh.name, Target.Address)
'    Zoom = ActiveWindow.Zoom
'    if Zoom <> 100 Then
'       Application.ScreenUpdating = False
'       Zoom = ActiveWindow.Zoom
'       ActiveWindow.Zoom = 100
'       ActiveWindow.Zoom = Zoom
'       Application.ScreenUpdating = True
'    End if
'    ActiveSheet.Range(Target.Address).Select 'Avoid jump to other sheet if the cell contain sheet pake name
'    Application.SendKeys ("{ESC}")
'End Sub
Rem =======================================================

Private Const alName As String = "_blue_or_red_arrow"
Private Const toColor As Long = &HFF0000  'RGB(255, 0, 0)
Private Const fromColor As Long = &HFF   'RGB(0, 0, 255)



Function LastRow(name) As Integer
    LastRow = Sheets(name).UsedRange.Rows.Count
End Function

Function LastCol(name) As Integer
    LastCol = Sheets(name).UsedRange.Columns.Count
End Function
Function cellLoc(cell) As Single()
    Dim p As Range
    Dim loc(0 To 1) As Single
    
    Set p = ActiveSheet.Range(cell)
    loc(0) = p.Left + p.Width / 2
    loc(1) = p.Top + p.Height / 2
    cellLoc = loc
End Function
Sub drawRLine(sCell, eCell, color)
    Dim ps, pe As Variant
    
    ps = cellLoc(sCell)
    pe = cellLoc(eCell)
    ActiveSheet.Shapes.AddLine(ps(0), ps(1), pe(0), pe(1)).Select
    Selection.name = alName
    Selection.ShapeRange.Line.EndArrowheadStyle = msoArrowheadOpen
    Selection.ShapeRange.Line.BeginArrowheadStyle = msoArrowheadOval
    With Selection.ShapeRange.Line
        .Visible = msoTrue
        .ForeColor.RGB = color
        .Transparency = 0
        .Weight = 1.5
    End With
End Sub
Function splitFormula(st) As Variant
    Dim tempSt, result, str1 As String
    Dim sq, dq, tq As Integer
    
    tempSt = Replace(st, "=", "")
    result = ""
    While Len(tempSt) > 0
        sq = InStr(tempSt, "'")
        dq = InStr(tempSt, """")
        If (sq < dq Or dq = 0) And sq > 0 Then
            str1 = Left(tempSt, sq - 1)
            tempSt = Mid(tempSt, sq)
            result = result & trimStr(str1)
            tq = InStr(2, tempSt, "'")
            result = result & Left(tempSt, tq)
            tempSt = Mid(tempSt, tq + 1)
        ElseIf (sq > dq Or sq = 0) And dq > 0 Then
            str1 = Left(tempSt, dq - 1)
            tempSt = Mid(tempSt, dq)
            result = result & trimStr(str1)
            tq = InStr(2, tempSt, """")
            result = result & Left(tempSt, tq)
            tempSt = Mid(tempSt, tq + 1)
        Else
            result = result & trimStr(tempSt)
            tempSt = ""
        End If
    Wend
    splitFormula = Split(result, Chr(27))
End Function

Function trimStr(tempSt) As String
    tempSt = Replace(UCase(tempSt), " AND ", Chr(27))
    tempSt = Replace(tempSt, " OR ", Chr(27))
    tempSt = Replace(tempSt, " NOT ", Chr(27))
    tempSt = Replace(tempSt, "+", Chr(27))
    tempSt = Replace(tempSt, "-", Chr(27))
    tempSt = Replace(tempSt, "*", Chr(27))
    tempSt = Replace(tempSt, "/", Chr(27))
    tempSt = Replace(tempSt, "(", Chr(27))
    tempSt = Replace(tempSt, ")", Chr(27))
    tempSt = Replace(tempSt, "^", Chr(27))
    'tempSt = Replace(tempSt, ":", Chr(27))
    tempSt = Replace(tempSt, "$", "")
    trimStr = Replace(tempSt, " ", "")
End Function
Sub getLines(shtName, cell)
Rem add cells in range
    Dim lastR, lastC, r, col, x  As Long
    Dim fRow, lRow, fCol, lCol, rr As Integer
    
    Dim wksht As Worksheet
    Dim shp As Shape
    Dim theRange As Range
    
    Dim theCells() As String
    Dim f As String
    
    Dim cc As Variant
    cell = Replace(UCase(cell), "$", "")
    cell = Split(cell, ":")(0)
        lastR = LastRow(shtName)
        lastC = LastCol(shtName)
        Set wksht = Sheets(shtName)
        wksht.Select
        Rem remove all sharp
        For Each shp In ActiveSheet.Shapes
            If shp.Type = msoLine And shp.name = alName Then
                shp.Delete
            End If
        Next shp
        For r = 1 To lastR
            For col = 1 To lastC
                f = Trim(CStr(wksht.Cells(r, col).Formula))
                If Len(f) > 0 And InStr(f, "=") = 1 Then
                    theCells = splitFormula(f)
                    For x = LBound(theCells, 1) To UBound(theCells, 1)
                        If InStr(theCells(x), ":") > 0 Then
                            If inRange(theCells(x), cell) Then
                                Call drawRLine(cell, Cells(r, col).Address, fromColor)
                            End If
                        End If
                        If Trim(theCells(x)) = cell Then
                            Call drawRLine(cell, Cells(r, col).Address, fromColor)
                            Exit For
                        End If
                    Next x
                End If
            Next col
        Next r
        
        f = Trim(CStr(wksht.Range(cell).Formula))
        If Len(f) > 0 And InStr(f, "=") = 1 Then
        theCells = splitFormula(f)
        For x = LBound(theCells, 1) To UBound(theCells, 1)
            
            If InStr(theCells(x), ":") > 0 Then
                Set theRange = Nothing
                On Error Resume Next
                Set theRange = Range(theCells(x))
                On Error GoTo 0
                If Not (theRange Is Nothing) Then
                    fRow = theRange.Row
                    fCol = theRange.Column
                    lRow = fRow + theRange.Rows.Count - 1
                    lCol = fCol + theRange.Columns.Count - 1
                    
                    For rr = fRow To lRow
                        For cc = fCol To lCol
                            Call drawRLine(wksht.Cells(rr, cc).Address, cell, toColor)
                        Next
                    Next
                End If
            End If
            cc = Null
            On Error Resume Next
            cc = wksht.Range(theCells(x)).Text
            On Error GoTo 0
            If Not IsNull(cc) And InStr(theCells(x), ":") <= 0 Then
                Call drawRLine(theCells(x), cell, RGB(0, 0, 255))
            End If
        Next x
    End If
End Sub
Function inRange_bak(ranges, cell) As Boolean
Rem test if the cell is in range
    Dim theRange, tr As Range
    Dim fRow, lRow, fCol, lCol, rr, cc As Integer

    Set theRange = Nothing
    On Error Resume Next
    Set theRange = Range(ranges)
    On Error GoTo 0
    If theRange Is Nothing Then
        inRange = False
    Else
        fRow = theRange.Row
        fCol = theRange.Column
        lRow = fRow + theRange.Rows.Count
        lCol = fCol + theRange.Columns.Count
    
        Set tr = Range(cell)
        If (tr.Row >= fRow And tr.Row < lRow And tr.Column >= fCol And tr.Column < lCol) Then
            inRange = True
        Else
           inRange = False
        End If
    End If
End Function
Function inRange(ranges, cell) As Boolean
Rem use the function
    Dim theRange, testRange, result As Range

    Set result = Nothing
    On Error Resume Next
    Set theRange = Range(ranges)
    Set testRange = Range(cell & ":" & cell)
    Set result = Intersect(theRange, testRange)
    On Error GoTo 0
    If result Is Nothing Then
        inRange = False
    Else
        inRange = True
    End If
End Function



